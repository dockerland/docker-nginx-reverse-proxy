FROM nginx

COPY nginx.conf /etc/nginx/nginx.conf
COPY dockers-ips /etc/nginx/dockers-ips
COPY ssl-cert/server.crt /etc/nginx/server.crt
COPY ssl-cert/server.key /etc/nginx/server.key

EXPOSE 80
EXPOSE 443