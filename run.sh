#!/bin/bash

if [ "$#" -eq  "2" ] 
then
    echo [LOG] "Two Arguments supplied. Setting ip number to "$1 and port number to $2
    ip_count=$1
    port_count=$2
elif [ "$#" -eq  "1" ] 
then
    echo [LOG] "One Arguments supplied. Setting ip number to "$1
    ip_count=$1
    port_count=1
else
    echo [LOG] "No arguments supplied. setting both ip and port number to 1"
    ip_count=1
    port_count=1
fi

docker_name=nginx-rp
docker_image_name=nginx-rp

echo [LOG] Remove dangling/untagged images
docker rmi $(docker images --quiet --filter "dangling=true")

echo [LOG] Removing $docker_name instances
docker ps -aq -f name=$docker_name | xargs docker rm -f

echo [LOG] Creating $ip_count ips with $port_count for each
rm -f dockers-ips
for ((i=1; i<=ip_count; i++)); do
    for ((j=0; j<port_count; j++)); do
        echo server 172.20.30.$((101+i)):$((80+j))\; >> dockers-ips
    done
done

echo [LOG] building $docker_image_name and running $docker_name afterward
docker build -t $docker_image_name . 

nginx_count=1
for ((i=0; i<nginx_count; i++)); do
    echo [LOG] Running a container named $docker_name from $docker_image_name
    docker run -d -p 443:443 -p 80:80 --name $docker_name-$i $docker_image_name
done

echo [LOG] "Successfully Done :-)"
docker ps